package s13;

import java.time.LocalDate;

public class Driver {

    @NotNull
    @NotEmpty

    @Min(lenghtMin = 3)
    private String firstName;

    @NotNull
    @NotEmpty

    @Min(lenghtMin = 3)
    private String lastName;

    @NotNull
    @NotEmpty

    @Min(lenghtMin = 3)
    private String patronymic;
    @NotNull
    @NotEmpty
    private LocalDate dateBorn;

    @NotNull
    @NotEmpty

    @Min(lenghtMin = 3)
    private String numberLicense;
    @NotNull
    @NotEmpty
    private LocalDate dateLicense;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setDateBorn(LocalDate dateBorn) {
        this.dateBorn = dateBorn;
    }

    public void setNumberLicense(String numberLicense) {
        this.numberLicense = numberLicense;
    }

    public void setDateLicense(LocalDate dateLicense) {
        this.dateLicense = dateLicense;
    }

}


