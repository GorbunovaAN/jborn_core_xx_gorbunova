package s13;

import java.time.LocalDate;

public class Tack1 {
    public static void main(String[] args) throws IllegalAccessException {
        Driver driver = new Driver();
        driver.setFirstName("Sasha");
        driver.setPatronymic("Gorbunova");
       driver.setLastName("hih");
        driver.setDateBorn(LocalDate.of(1995, 4, 29));
       driver.setDateLicense(LocalDate.of(2022, 7, 4));
        driver.setNumberLicense("567654");

        Validator validator = new Validator();
        if (validator.validate(driver)) {
            System.out.println("Данные приняты!" );
        } else {
            System.out.println("Данные не приняты!");
        }
    }
}