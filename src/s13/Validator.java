package s13;

import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Pattern;

public class Validator {

    boolean validate(Object obj) throws IllegalAccessException {

        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);

            if (field.isAnnotationPresent(NotNull.class) && (Object.class.isAssignableFrom(field.get(obj).getClass()))) {

                if (field.get(obj) == null) return false;
            }

            if (field.isAnnotationPresent(Max.class) && (String.class.isAssignableFrom(field.get(obj).getClass()))) {
                Max max = field.getAnnotation(Max.class);
                if (field.get(obj).toString().length() > max.lenghtMax()) return false;
            }
            if (field.isAnnotationPresent(Min.class) && (String.class.isAssignableFrom(field.get(obj).getClass()))) {
                Min min = field.getAnnotation(Min.class);
                if (field.get(obj).toString().length() < min.lenghtMin()) return false;
            }
            if (field.isAnnotationPresent(NotEmpty.class) && (Collections.class.isAssignableFrom(field.get(obj).getClass()))) {
                if (field.get(obj).toString().isEmpty()) return false;
            }
            if (field.isAnnotationPresent(Regexp.class) && (String.class.isAssignableFrom(field.get(obj).getClass()))) {
                Regexp regexp = field.getAnnotation(Regexp.class);
                if (!Pattern.matches(regexp.regexp(), field.get(obj).toString())) return false;
            }
        }
        return true;
    }

}