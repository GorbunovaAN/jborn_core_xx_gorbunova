package s06;

public class Tack1 {
    public static void main(String[] args) {


        Rectangle rectangle = new Rectangle(5, 10);

        System.out.println(rectangle.calculatePerimeter());
        System.out.println(rectangle.calculateArea());

        Square square = new Square(3);
        System.out.println(square.calculatePerimeter() + " ________");
        System.out.println(square.calculateArea() + " --------");

        Triangle triangle = new Triangle(7, 7, 7);
        System.out.println(triangle.calculatePerimeter());

        System.out.println(triangle.calculateArea());

    }
}