package s06;

interface Handler {
    String handleMessage(String message);
}