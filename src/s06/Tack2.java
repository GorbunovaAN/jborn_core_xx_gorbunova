package s06;

public class Tack2 {
    public static void main(String[] args) {

        Carriage[] carriages = new Carriage[]{
                new Carriage(2),
                new Carriage(5),
                new Carriage(4)
        };

        RailwayTrain r = new RailwayTrain(carriages);

        r.printStatus();
    }
}