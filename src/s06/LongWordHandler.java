package s06;

public class LongWordHandler implements Handler {

    int min;
    int max;

    public LongWordHandler(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public String handleMessage(String message) {

        if (message != null) {
            int c = message.split(" ").length;
            if (c >= min && c <= max) {
                return message;
            }
        }
        return null;

    }
}

