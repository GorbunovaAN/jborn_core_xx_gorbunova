package s06;

    public class Triangle extends Shape {
        private double width;
        private double height;

        private double base;

        public double getBase() {
            return base;
        }

        public double getWidth() {
            return width;
        }

        public double getHeight() {
            return height;
        }


        Triangle(double x, double y, double z) {

            this.width = x;
            this.height = y;
            this.base = z;
        }


        public double calculatePerimeter() {

            return getWidth() + getHeight() + getBase();
        }

        public double calculateArea() {
            double p = (getWidth() + getHeight() + getBase()) / 2;
            double area = Math.sqrt(p * (p - getWidth()) * (p - getHeight()) * (p - getBase()));
            return area;
        }
    }


