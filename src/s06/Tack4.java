package s06;

public class Tack4 {
    public static void main(String[] args) {
        LowerCaseHandler lowerCaseHandler = new LowerCaseHandler();
        AlphabetHandler alphabetHandler = new AlphabetHandler();
        LongWordHandler longWordHandler = new LongWordHandler(4,6);
        System.out.println(longWordHandler.handleMessage(alphabetHandler.handleMessage(lowerCaseHandler.handleMessage("   " ))));

    }
}