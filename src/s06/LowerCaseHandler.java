package s06;


public class LowerCaseHandler implements Handler {

    @Override
    public String handleMessage(String message) {
        if (message != null) {
            return message.toLowerCase().trim();
        }
        return null;
    }
}






