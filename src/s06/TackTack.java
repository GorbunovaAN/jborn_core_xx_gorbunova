package s06;

public class TackTack {
    public static void main(String[] args) {

        Shape[] shapes = {
                new Rectangle(5, 10),
                new Square(3),
                new Triangle(7, 7, 7)
        };

        myMethod(shapes);
    }

    static void myMethod(Shape[] shapes) {
        double sum = 0;
        for (int i = 0; i < shapes.length; i++) {
            sum += shapes[i].calculateArea();
        }
        System.out.println(sum + "  - суммарная площадь ");
    }
}


