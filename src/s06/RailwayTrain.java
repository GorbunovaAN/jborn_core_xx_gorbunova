package s06;

public class RailwayTrain {     //состав

    private Carriage[] carriages;


    public RailwayTrain(Carriage[] carriages) {
        this.carriages = carriages;
    }

    public float getGeneral() {
        float result = 0;
        for (int i = 0; i < carriages.length; i++) {
            result = result + carriages[i].getTonna(i);
        }
        return result;
    }

    public float getAverage() {

        return getGeneral() / carriages.length;
    }

    public void printStatus() {

        for (int i = 0; i < carriages.length; i++) {
            System.out.println(carriages[i]);
        }

        System.out.println("среднее " + getAverage() + "   общее" + getGeneral());
    }
}

