package s06;


class Rectangle extends Shape {

    private double width;
    private double height;

    Rectangle(double x, double y) {
        this.width = x ;
        this.height = y;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public double calculatePerimeter() {

        return (getWidth() + getHeight()) * 2;
    }

    public double calculateArea() {

        return getWidth() * getHeight();
    }
}
