package s06;

public class AlphabetHandler implements Handler {

    @Override
    public String handleMessage(String message) {

        var result = "";

        if (message != null) {

            if (message.matches("[а-я ]+")) {
                result = message;
            } else {
                result = null;
            }
        }
        return result;
    }

}





