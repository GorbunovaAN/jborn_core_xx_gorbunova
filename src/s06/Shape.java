package s06;

abstract class Shape {

    public abstract double calculatePerimeter();

    public abstract double calculateArea();
}