package s06;

//вагон
public class Carriage {

    private float tonna;


    public Carriage(float tonna) {
        this.tonna = tonna;

    }

    public float getTonna(int i) {
        return tonna;
    }

    @Override
    public String toString() {
        return "Carriage{" +
                "tonna=" + tonna +
                '}';
    }
}