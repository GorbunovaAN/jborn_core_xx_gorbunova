package s07;

    public class Person {
        String name;
        int age;
        int growth;

        public Person(final String name, final int age, final int growth) {
            this.name = name;
            this.age = age;
            this.growth = growth;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ", growth=" + growth +
                    '}';
        }
    }

