package s07;

public class Driver {



    private String name;
    private int age;
    private int growth;


    public Driver(final String name, final int age, final int growth) {
        this.name = name;
        this.age = age;
        this.growth = growth;
    }
    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }
    public int getGrowth() {
        return growth;
    }
}
