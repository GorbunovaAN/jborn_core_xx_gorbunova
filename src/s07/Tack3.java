package s07;

import java.math.BigDecimal;

public class Tack3 {
    public static void main(String[] args) {


        System.out.println(compare(5, 5.00000001));

    }

    static <T extends Number> int compare(T x, T y) {

        BigDecimal decimalOne = new BigDecimal(x.toString());
        BigDecimal decimalTwo = new BigDecimal(y.toString());
        return decimalOne.compareTo(decimalTwo);

    }

}
