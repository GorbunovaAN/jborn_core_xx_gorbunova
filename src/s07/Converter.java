package s07;

public interface Converter<T, N> {
    N convert(T t);

}