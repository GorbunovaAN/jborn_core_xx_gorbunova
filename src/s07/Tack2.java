package s07;

public class Tack2 {
    public static void main(String[] args) {
        Driver d = new Driver("Леха", 5, 176);

        Converter<Driver, Person> converter = x -> new Person(x.getName(), x.getAge(), x.getGrowth());

        Person raccoon = converter.convert(d);

        System.out.println(raccoon);

    }
}
