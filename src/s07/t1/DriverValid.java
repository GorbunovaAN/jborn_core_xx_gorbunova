package s07.t1;

public class DriverValid implements Validator<Driver> {

    @Override
    public boolean isValid(Driver driver) {
        if (driver == null) {
            return false;
        }
        if (driver.getName() == null) {

            return false;
        }
        if (driver.getSurname() == null) {

            return false;
        }
        if (driver.getPatronymic() == null) {

            return false;
        }
        if (driver.getDateOfBirth() == null) {

            return false;
        }
        if (driver.getLicenseNumber() == null) {

            return false;
        }
        if (driver.getDateOfIssue() == null) {

            return false;
        }

        return true;
    }

}



