package s07.t1;

public interface Validator<T> {

    boolean isValid(T ob);
}

