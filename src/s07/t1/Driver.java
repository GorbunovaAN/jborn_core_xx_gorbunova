package s07.t1;

public class Driver {
    String name;
    String surname;
    String patronymic;
    String dateOfBirth;
    String licenseNumber;
    String dateOfIssue;

    public Driver(String name, String surname, String patronymic, String dateOfBirth, String licenseNumber, String dateOfIssue) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.dateOfBirth = dateOfBirth;
        this.licenseNumber = licenseNumber;
        this.dateOfIssue = dateOfIssue;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getDateOfBirth() {
        return dateOfBirth;

    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public String getDateOfIssue() {
        return dateOfIssue;

    }


}


