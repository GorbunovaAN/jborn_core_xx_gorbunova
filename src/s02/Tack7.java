package s02;

import java.util.Scanner;

import static java.lang.Math.sin;

public class Tack7 {
    public static void main(String args[]) {

        System.out.print("Введите значение Х: ");

        double x = sc.nextDouble();
        double y;

        if (x > 0) {
            y = sin(x) * sin(x);
        } else {
            y = 1 - (2 * sin(x * x));
        }
        System.out.println("Y=" + y);
    }

    static Scanner sc = new Scanner(System.in);

}

