package s02;

import java.util.Scanner;

public class Task2 {

    public static void main(String[] args) {
        int n = requestNumber();
        int sec = n % 60;
        int min = n % 3600 / 60;
        int ho = n % 86400 / 3600;
        System.out.println(ho + " " + min + " " + sec);

    }

    static int requestNumber() {
        Scanner s = new Scanner(System.in);
        System.out.println("введите число секунд");
        return s.nextInt();
    }
}
