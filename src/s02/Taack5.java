package s02;

import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Math.pow;

public class Taack5 {


        public static void main (String[]args){

            int[] numbers = new int[3];
            numbers[0] = requestNumber();
            numbers[1] = requestNumber();
            numbers[2] = requestNumber();


            Arrays.sort(numbers);

            if ((pow(numbers[0], 2) + pow(numbers[1], 2)) == pow(numbers[2], 2)) {
                System.out.println("Эти числа являются тройкой Пифагора");
            } else {
                System.out.println("Эти числа не являются тройкой Пифагора");
            }
        }

        static int requestNumber () {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите число:");
            return scanner.nextInt();
        }

    }

