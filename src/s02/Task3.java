package s02;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {

        int n = requestNumber();
        int m = n / 10;
        int c = n % 10;
        int x = c * m;
        int y = c + m;
        System.out.println("Десятки" + m + " " + "Единицы" + c);
        System.out.println("Произведение цифр" + x);
        System.out.println("Сумма цифр" + y);
    }

    static int requestNumber() {
        Scanner s = new Scanner(System.in);
        System.out.println("введите число");
        return s.nextInt();

    }
}

