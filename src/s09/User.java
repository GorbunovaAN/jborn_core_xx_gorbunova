package s09;
import java.io.*;

class User implements Externalizable {     // сделать класс Externalizable


    private String firstName;
    private transient String lastName;

    private static String department;


    // объект создается с помощью общедоступного конструктора без аргументов перед вызовом метода readExternal.
    // нет конструктора, во время выполнения генерируется исключение InvalidClassException.
    public User() {
    }

    // Конструктор All-arg для создания объектов вручную
    public User(String firstName, String lastName, String department) {
        this.firstName = firstName;
        this.lastName = lastName;

        User.department = department;
    }

    @Override
    //указать, что сериализовать в методе writeExternal().
    public void writeExternal(ObjectOutput out) throws IOException {

        out.writeUTF(firstName);
        out.writeUTF(lastName);

        out.writeUTF(department);
    }

    @Override
    // нужно указать, что десериализовать в методе readExternal()
    // Метод readExternal должен считывать значения в той же последовательности и с теми же типами, которые были записаны методом writeExternal
    public void readExternal(ObjectInput in) throws IOException {

        firstName = in.readUTF();
        lastName = in.readUTF();

        department = in.readUTF();
    }

    @Override
    public String toString() {
        return "User{" +
                "firstname='" + firstName + '\'' +
                ", lastname='" + lastName + '\'' +
                '}';
    }

}
