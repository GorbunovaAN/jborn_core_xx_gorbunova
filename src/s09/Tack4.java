package s09;

import java.io.*;

public class Tack4 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

        User user = new User("Shanti", "Sharma", "IT");
        System.out.println("Object before serialization  => " + user.toString());

        // Serialization
        serialize(user);

        // Deserialization
        User deserializedEmpObj = deserialize();
        System.out.println("Object after deserialization => " + deserializedEmpObj.toString());
    }

    // метод сериализация
    static void serialize(User user) throws IOException {
        try (FileOutputStream fos = new FileOutputStream("/home/alex/IdeaProjects/jborn_core_xx_gorbunova/src/us");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(user);
        }
    }

    // метод десериализация
    static User deserialize() throws IOException, ClassNotFoundException {
        try (FileInputStream fis = new FileInputStream("/home/alex/IdeaProjects/jborn_core_xx_gorbunova/src/us");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            return (User) ois.readObject();
        }
    }
}
