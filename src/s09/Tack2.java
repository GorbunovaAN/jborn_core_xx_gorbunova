package s09;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Tack2 {
    public static void main(String[] args) throws IOException {

        int[] array = new int[10];
        StringBuilder ar = new StringBuilder();
        Random rand = new Random((10 - (-10)) + (-10));

        for (var q = 0; q < array.length; ++q) array[q] = rand.nextInt(50);
        for (var q = 1; q < array.length; q += 2) array[q] = -array[q];

        for (int i : array) {
            ar.append(i + " ");
        }
        writeArray("/home/alex/IdeaProjects/untitled1/src/sasha", ar.toString());


        StringBuilder positive = new StringBuilder();
        StringBuilder negative = new StringBuilder();
        for (int i : array) {
            if (i >= 0) {
                positive.append(i + " ");

            } else {
                negative.append(i + " ");
            }
        }
        writeArray("/home/alex/IdeaProjects/untitled1/src/positive_numbers", positive.toString());
        writeArray("/home/alex/IdeaProjects/untitled1/src/negative_numbers", negative.toString());

    }

    public static void writeArray(String fileName, String array) throws IOException {
        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write(array);

        }
    }


}


