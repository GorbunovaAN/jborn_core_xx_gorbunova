package s09;

import java.io.*;
import java.nio.charset.Charset;

public class Tack3 {
    public static void main(String[] args) {

        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(new byte[]{72, 105, 33, 33, 33})) {


            String str = readAsString(inputStream, Charset.forName("UTF-8"));


            System.out.println(str);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static String readAsString(InputStream inputStream, Charset charset) throws IOException {
        String retval;
        try (Reader reader = new InputStreamReader(inputStream, charset)) {
            StringWriter r = new StringWriter();
            int b;
            while ((b = reader.read()) != -1) {
                r.write(b);
            }

            retval = r.toString();
            return retval;
        }
    }
}
