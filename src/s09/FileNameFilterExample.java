package s09;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileNameFilterExample {


    private static List result = new ArrayList();//Инициализируем контейнер, куда будем складывать адреса файлов, которые удовлетворяют условию задачи


    public static void listFiles(String currentDir, String name) {
        File dir = new File(currentDir);
        File[] files = dir.listFiles();//Получаем все каталоги и файлы в текущем каталоге

        if (files != null && files.length > 0) {
            for (File file : files) { //Перебираем все каталоги и файлы, находящиеся в текущем каталоге

                if (file.isFile() && file.getAbsolutePath().contains(name)) { //Если объект является файлом (не папкой) и в его пути содержится ключечевое слово, по которому ищем файлы, то добавляем адрес файла в результат

                    result.add(file.getAbsoluteFile());//Добавляем в результирующий список файлов текущий файл
                } else {
                    listFiles(file.getAbsolutePath(), name);//Если объект является не файлом, а каталогом, то вызываем рекурсивно для него метод
                }
            }
        }
    }

    public static void main(String[] args) {
        listFiles("/home", "Tack4"); //Вызываем метод поиска файлов, передаем в него каталог и имя, которое должно содержаться в файлах
        result.forEach(System.out::println);//Печатаем результат в консоль
    }

}


























//    public void checkNoOfFiles(String filename) throws IOException {
//
//        File dir = new File(filename);
//        File files[] = dir.listFiles();//Массив файлов хранит список файлов
//
//        for (int i = 0; i < files.length; i++) {
//            if (files[i].isFile()) //проверить, является ли files[i] файлом или каталогом
//            {
//                System.out.println("File::" + files[i].getName());
//                System.out.println();
//
//            } else if (files[i].isDirectory()) {
//                System.out.println("Directory::" + files[i].getName());
//                System.out.println();
//                checkNoOfFiles(files[i].getAbsolutePath());
//            }
//        }
//    }
//
//    public static void main(String[] args) throws IOException {
//
//        FileNameFilterExample mf = new FileNameFilterExample();
//        String str = "/home/alex/IdeaProjects/jborn_core_xx_gorbunova/src";
//        mf.checkNoOfFiles(str);
//    }
//}
