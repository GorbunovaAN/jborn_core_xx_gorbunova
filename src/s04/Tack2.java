package s04;

import java.io.StringWriter;

public class Tack2 {
    public static void main(String[] args) {

        String str = "hi hi";
        System.out.println(rec(str));
    }


    public static String rec(String str) {
        if (str.length() <= 1) {
            return str;
        }
        return rec(str.substring(1)) + str.charAt(0);
    }


}


