package s04;

public class Tack3 {
    public static void main(String[] args) {
        System.out.println(fib(-10));
    }

    public static int fib(int f) {

        if (f == 0) {
            return 0;
        }

        if (f == 1) {
            return 1;
        }

        if (f < 0) {
            return fib(-f) * (int) Math.pow(-1, f + 1);
        }

        return fib(f - 1) + fib(f - 2);
    }
}