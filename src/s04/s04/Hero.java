package s04.s04;

import java.util.Collections;

public class Hero {

    static int INITIAL_HEALTH = 100;

    private int healt;
    private final int power;
    private final int speed;
    private static String name = " Игорь";

    public Hero(int power) {
        this.healt = INITIAL_HEALTH;
        this.power = power % 100;
        this.speed = 100 - this.power;
    }

    public static String getName() {
        return name;
    }

    public int getHealt() {
        return healt;
    }

    public int getPower() {
        return power;
    }

    public int getSpeed() {
        return speed;
    }

    public void hit(Hero hero) {
        hero.healt = hero.healt - this.power;


    }

    public void printStatus() {

        System.out.println("health \t" + repeatAsterisk(healt / 10) + "\n"
                + "power\t" + repeatAsterisk(power / 10) + "\n" +
                "speed\t" + repeatAsterisk(speed / 10) + "\n");
    }

    private static String repeatAsterisk(int times) {

        return String.join("", Collections.nCopies(times, "*"));
    }
}

