package s04;

public class Tack4 {
    public static void main(String[] args) {
        Circle ro = new Circle(1);
        System.out.println(ro.squareOfCircle());
        ro.setCenterOfCircle(1, 0);

        Circle go = new Circle(1);
        go.setCenterOfCircle(-1.00001, 0);
        System.out.println(go.squareOfCircle());
        System.out.println(ro.distanceBetweenCenter(go));

        System.out.println(go.isIntersect(ro));
        System.out.println(go.isIntersect(go));
    }
}


