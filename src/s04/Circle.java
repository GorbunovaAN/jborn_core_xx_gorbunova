package s04;


public class Circle {
    private double radius;
    private double x = 0;
    private double y = 0;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double squareOfCircle() { //площадь круга
        return Math.PI * this.radius * this.radius;
    }

    public void setCenterOfCircle(double x, double y) { // установить центр круга
        this.x = x;
        this.y = y;
    }

    public double distanceBetweenCenter(Circle circle) { // расстояние между центром кругов
        double x = Math.abs(circle.x - this.x);
        double y = Math.abs(circle.y - this.y);

        return Math.sqrt(x * x + y * y);
    }

    public boolean isIntersect(Circle circle) {   // пересечение кругов
        return distanceBetweenCenter(circle) <= (circle.getRadius() + this.radius);
    }

    private double getRadius() { //получить радиус
        return radius;
    }

}