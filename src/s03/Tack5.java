package s03;

import java.util.Scanner;

public class Tack5 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("напишите слово");
        String str = s.nextLine();

        StringBuilder newStr = new StringBuilder(str);
        newStr.reverse();

        if (str.equals(newStr.toString())) {
            System.out.println("является");
        } else
            System.out.println("нет");
    }
}
