package s03;

import java.util.Scanner;

public class Tack4 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("введите число");
        int x = s.nextInt();
        int y = s.nextInt();

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if ((i == 0 || i == x - 1) || (j == 0 || j == y - 1)) {
                    System.out.print('*');
                } else {
                    System.out.print(' ');
                }
            }
            System.out.print("\n\r");
        }
    }
}
