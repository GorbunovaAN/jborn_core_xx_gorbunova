package s03;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Tack6 {
    public static void main(String[] args) {
        System.out.println("Введите строку:");
        Scanner scanner = new Scanner(System.in);
        int count = 0;

        String a = scanner.nextLine();
        System.out.println("Введите символ:");
        char c = scanner.next().charAt(0);

        char result[] = a.toCharArray();


        for (int i = 0; i < result.length; i++) {
            if (result[i] == c) {
                count++;

                result[i] = Character.toUpperCase(result[i]);
            }
        }
        String resStr = new String(result);
        System.out.println("Кол-во вхождений:" + count);
        System.out.println(resStr);


    }
}



