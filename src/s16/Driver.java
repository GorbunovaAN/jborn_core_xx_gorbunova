package s16;

public class Driver {
    private final String firstName;
    private final String lastName;
    private final String numberLicense;

    public Driver(String firstName, String lastName, String numberLicense) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.numberLicense = numberLicense;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNumberLicense() {
        return numberLicense;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", numberLicense='" + numberLicense + '\'' +
                '}';
    }
}