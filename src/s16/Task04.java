package s16;

import java.util.*;
import java.util.stream.Collectors;

public class Task04 {
    public static void main(String[] args) {
        List<List<String>> list = Arrays.asList(
                Arrays.asList("Ваня", "Аня", null, "Александр"),
                Arrays.asList("Дима", null, "Артемида", "Коля"),
                Arrays.asList("Антон", "Афродита", "Сергей")
        );

        System.out.println(list.stream()
                .flatMap(Collection::stream).filter(str -> ((str != null) && (str.charAt(0) == 'С'))).
                sorted(Comparator.reverseOrder()).collect(Collectors.joining(",")));
    }
}