package s16;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class Task03 {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            integers.add((int) ((Math.random() * 200) - 100));
        }
        System.out.println(integers);

        System.out.println("Сумма = " + IntStream.range(0, integers.size())
                .filter(index -> (index % 2 == 0)).map(integers::get).sum());

    }
}
