package s16;

public @interface FunctionalInterface {

    public interface Validator<T> {
        public boolean isValid(T obj);
    }
}
