package s16;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class Task01 {
    public static void main(String[] args) {

        List<Driver> drivers = asList(
                new Driver(null, "Игнат", "1"),
                new Driver("Игорь", "Петров", "8635789366923"));

        FunctionalInterface.Validator<Driver> validator = driver -> ((driver.getFirstName() != null && driver.getFirstName().length() > 3) &&
                (driver.getLastName() != null && driver.getLastName().length() > 3) &&
                (driver.getNumberLicense().length() > 8));

        List<Driver> driverList = drivers.stream().filter((validator::isValid)).collect(Collectors.toList());

        System.out.println(driverList);

    }
}