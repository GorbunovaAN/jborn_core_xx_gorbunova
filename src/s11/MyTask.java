package s11;

public class MyTask<T extends Comparable> {
    public static void main(String[] args) {
        MyTask f = new MyTask();

        f.add(4);
        f.add(5);
        f.add(7);
        f.add(2);
        f.add(10);
        f.add(6);

        f.printAll();
    }
    public class Node<T extends Comparable<T>> {
        T data;
        Node parent;
        Node left;
        Node right;

        public Node(T data) {
            this.data = data;
            this.left = null;
            this.right = null;
            this.parent = null;
        }

    }

    Node ROOT;

    public MyTask() {
        this.ROOT = null;
    }

    void add(T data) {
        insertNode(this.ROOT, data);
    }

    void insertNode(Node node, T data) {

        if (node == null) {
            node = new Node(data);
            ROOT = node;
        } else if (node.data.compareTo(data) == 1 && node.left == null) {
            node.left = new Node(data);
            node.left.parent = node;
        } else if (node.data.compareTo(data) < 1 && node.right == null) {
            node.right = new Node(data);
            node.right.parent = node;
        } else {
            if (node.data.compareTo(data) == 1) {
                insertNode(node.left, data);
            } else {
                insertNode(node.right, data);
            }
        }
    }

    public void printAll() {
        printInOrder(this.ROOT);
    }

    public void printInOrder(Node node) {

        if (node != null) {
            printInOrder(node.left);
            System.out.print(node.data + ", ");
            printInOrder(node.right);
        }
    }

}