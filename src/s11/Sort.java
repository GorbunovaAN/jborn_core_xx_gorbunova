package s11;

import java.util.Arrays;

public class Sort {
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    // Разделение
    public static int partition(int[] a, int start, int end) {
        //  крайний правый элемент в качестве опорного элемента массива
        int pivot = a[end];

        // элементы меньшие будут перемещены влево от `pIndex`
        // элементы больше будут сдвинуты вправо от `pIndex`
        // равные элементы могут идти в любом направлении
        int pIndex = start;

        // каждый раз, когда мы находим элемент, меньший или равный опорному,
        // `pIndex` увеличивается, и этот элемент будет помещен
        // перед разворотом.
        for (int i = start; i < end; i++) {
            if (a[i] <= pivot) {
                swap(a, i, pIndex);
                pIndex++;
            }
        }

        // поменять местами `pIndex` с пивотом
        swap(a, end, pIndex);

        // вернуть `pIndex` (индекс опорного элемента)
        return pIndex;
    }


    public static void quicksort(int[] a, int start, int end) {

        if (start >= end) {
            return;
        }

        // переставить элементы
        int pivot = partition(a, start, end);

        // повторяемся на подмассиве, содержащем меньше элементов, чем опорный
        quicksort(a, start, pivot - 1);

        // повторяем подмассив, содержащий элементов больше, чем опорный
        quicksort(a, pivot + 1, end);
    }


    public static void main(String[] args) {
        int[] a = {9, -3, 5, 2, 6, 8, -6, 1, 3};

        quicksort(a, 0, a.length - 1);

        System.out.println(Arrays.toString(a));
    }

}
