
package s10;

public interface List <T> {
    T get(int i);   //получить доступ по индексу
    void put(T e);   // добавление элемента по содержимому в конец
    void put(int i, T e);// добавление на место и . с таким сожержанием
    void remove(int i);//   удалить по индексу
    void remove(T e);//  удалить по содержимому
    int length();//
}

