package s10;

public class Tack<T> implements List<T> {
    private class Node<T> {

        private Tack.Node previous = this;
        private Tack.Node next = this;
        private T value;

        Node(T value) {
            this.value = value;
        }

        public String toString() {
            return value.toString();
        }
    }

    private Tack.Node head = new Tack.Node(null);


    private int size;

    @Override
    public T get(int index) {
        return getNode(index).value;

    }

    @Override
    public void put(T e) {
        addlast(e);
    }

    @Override
    public void put(int i, T e) {
        addBefore(new Tack.Node(e), getNode(i));

    }

    private Node<T> getNode(int index) {// Получить узел в указанной позиции
        if (index < 0 || index > size)
            throw new IndexOutOfBoundsException("The Index Outof Bounds");
        if (index < size / 2) {
            Tack.Node node = head;
            for (int i = 0; i <= index; i++) {
                node = node.next;
            }
            return node;
        } else {
            Tack.Node node = head;
            for (int i = size - 1; i >= index; --i) {
                node = node.previous;
            }
            return node;
        }
    }

    private boolean addlast(T value) {// Добавить позже
        addBefore(new Tack.Node(value), head);
        return true;
    }

    private void addBefore(Tack.Node newNode, Tack.Node node) {// Добавить перед узлом
        newNode.next = node;
        newNode.previous = node.previous;
        newNode.previous.next = newNode;
        newNode.next.previous = newNode;
        size++;
    }

    @Override
    public void remove(int i) {
        removeNode(getNode(i));

    }

    @Override
    public void remove(T e) {      //проитерироваться и сравнить каждое значение ноды с Т, если совпало удалить
        Node<T> current = head.next;
        Node<T> next;
        int i = 0;
        int mainSize = size;
        while (i < mainSize) {
            next = current.next;
            if (e.equals(current.value)) {
                removeNode(current); // в этом методе удаляется current.next поэтому если мы в строчке 101 хотим использовать current.next - то его уже не существует
            }
            i++;
            current = next;
        }

    }


    private void removeNode(Tack.Node node) {// Удалить узел
        if (size == 0)
            throw new IndexOutOfBoundsException("LinkedList is Empty");
        node.previous.next = node.next;
        node.next.previous = node.previous;
        node.next = null;
        node.previous = null;
        --size;
    }

    @Override
    public int length() {
        return size;
    }

    public static void main(String[] args) {
        Tack t = new Tack();

        t.put(15);
        t.put(new Integer(14));
        t.put(new Integer(2));
        t.put(new Integer(7));
        t.put(new Integer(2));
        t.put(3);
        t.put(new Integer(2));
        t.put(new Integer(8));
        t.put(new Integer(2));
        t.put(new Integer(2));
        t.put(new Integer(2));
        t.put(new Integer(2));
        t.put(new Integer(7));
        t.put(6);
        t.put(new Integer(1));
        t.remove(new Integer(2));


        for (int i = 0; i < t.size; i++) {
            System.out.println (t.getNode(i) + " remove OBJ");

        }
        System.out.println(t.get(7) + " индекс");

    }
}