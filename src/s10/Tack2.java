package s10;

public class Tack2 implements Queue<Integer> {
    private static Node rear = null, front = null;
    private static int count = 0;
    private Node head = null;

    @Override
    public boolean add(Integer e) {
        int item = e;
        // выделяем новый узел в куче
        Node node = new Node(item);
        System.out.printf("Inserting %d\n", item);

        // особый случай: queue пуста
        if (front == null) {
            // инициализируем и переднюю, и заднюю часть
            front = node;
            rear = node;
        } else {
            // обновить заднюю часть
            rear.next = node;
            rear = node;
        }

        return false;
    }

    @Override
    public Integer poll() {    // возвращает эл из начала очереди и удаляет его
        // Вспомогательная функция для удаления переднего элемента из очереди

        if (front == null) {
            System.out.println("\nQueue Underflow");
            return null;
        }

        Node temp = front;

        // продвигаемся вперед к следующему узлу
        front = front.next;
        return temp.data;
    }

    @Override
    public Integer peek() {  //возвращает эл из начала очереди без удалени

        // проверяем наличие пустой queue
        if (front == null) {
            return null;
        }

        return front.data;
    }


    public static void main(String[] args) {
        s10.Tack2 q = new s10.Tack2();
        q.add(1);
        q.add(2);
        q.add(3);
        q.add(4);
        System.out.println(q.poll() + "---1");
        System.out.println(q.poll() + "---2");
        System.out.println(q.peek());

    }
}

