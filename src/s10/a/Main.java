package s10.a;

public class Main {
    public static void main(String[] args) {
        Node root = generateListWithLoop();

        System.out.println(hasLoop(root));
    }

    private static Node generateListWithLoop() {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);

        n1.setNext(n2);
        n2.setNext(n3);
        n3.setNext(n4);
        n4.setNext(n5);
        n5.setNext(n3);

        return n1;
    }

    private static boolean hasLoop(Node root) {
        if (root == null)
            return false;

        Node tortoise = root;
        Node hare = root;

        while (true) {
            tortoise = tortoise.getNext();

            if (hare.getNext() != null)
                hare = hare.getNext().getNext();
            else
                return false;

            if ((tortoise == null) || (hare == null))
                return false;

            if (tortoise == hare)
                return true;
        }
    }
}