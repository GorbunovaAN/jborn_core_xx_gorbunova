package s8;

import java.io.*;
import java.math.BigInteger;
import java.util.Arrays;

public class Tack1 {

    private static String[] extentions = new String[]{"txt", "txt1"};

    static void writeToFirst(File file, String value) throws IOException {  //запись строки в файл
        FileWriter writer = new FileWriter(file, false);
        writer.write(value);
        writer.flush();
        writer.close();
    }


    static boolean isContainsChar(String s) {

        return s.matches("[А-Яа-я]+");
    }

    private static boolean isValidExtention(File file) {
        return Arrays.stream(extentions)
                .filter(ext -> ext.equals(getExtensionByStringHandling(file.getAbsolutePath())))
                .count() == 1;
    }

    public static String getExtensionByStringHandling(String filename) {
        return filename.substring(filename.lastIndexOf(".") + 1);
    }

    public static void main(String[] args) {

        File file = new File("/home/alex/IdeaProjects/jborn_core_xx_gorbunova/src/s8/sasha.txt2");

        BigInteger x = BigInteger.valueOf(Long.MAX_VALUE);
        try {
            if (!isValidExtention(file)) {
                throw new ExtentionFormatException("Неподдерживаемый формат файла");
            }

            String s = "9223372036854775806 1a 1";
            writeToFirst(file, s);


            if (file.exists() && file.canRead()) {//существует файл или нет //доступен для чтения или нет

                String[] array = s.split(" ");    // посчитать симму
                BigInteger su = new BigInteger("0");//ToDo
                for (int i = 0; i < array.length; ++i) {
                    if (isContainsChar(array[i])) {
                        throw new NumberFormatException("Содержит не только цифры");
                    }
                    BigInteger bigInteger = new BigInteger(array[i]);
                    su = su.add(bigInteger);
                    System.out.println("---- " + su);

                    if (su.compareTo(x) > 0) {
                        System.out.println(su);
                        throw new MaxValueException();
                    }
                }
            }
        } catch (ExtentionFormatException | NumberFormatException e) {
            e.getMessage();
            e.printStackTrace();
        } catch (IOException e) {      // исключение для writeToFirst и readFirstLine
            e.printStackTrace();
            System.out.println("ошибка прочтения файла");
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("строка пустая");
        } catch (MaxValueException e) {
            e.printStackTrace();
            System.out.println("слишком большое число " + e);
        }
    }
}


