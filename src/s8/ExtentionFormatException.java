package s8;

public class ExtentionFormatException extends Exception {

    public ExtentionFormatException(String s) {
        super(s);
    }
}
