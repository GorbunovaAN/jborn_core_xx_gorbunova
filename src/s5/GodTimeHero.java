package s5;

public class GodTimeHero extends Her0 {

    int godMinutes;
    private long startTime;

    public GodTimeHero(int power, int godMinutes) {
        super(power);
        this.startTime = System.currentTimeMillis();
        this.godMinutes = godMinutes;

    }

    @Override
    public void takeDamage() {
        System.out.println(godMinutes + "<<<godMinutes");

        if (System.currentTimeMillis() - startTime > godMinutes)
            super.takeDamage();

    }
}