package s5;

import java.util.Collections;

public class Her0 {
    static int INITIAL_HEALTH = 100;

    protected int healt;
    protected final int power;
    protected final int speed;


    public Her0(int power) {
        this.healt = INITIAL_HEALTH;
        this.power = power % 100;
        this.speed = 100 - this.power;
    }


    public void takeDamage() {
        this.healt = this.healt - this.power;
    }

    public void hit(Her0 hero) {
        hero.takeDamage();

    }

    public void printStatus() {

        System.out.println("health \t" + repeatAsterisk(healt / 10) + "\n"
                + "power\t" + repeatAsterisk(power / 10) + "\n" +
                "speed\t" + repeatAsterisk(speed / 10) + "\n");
    }

    private static String repeatAsterisk(int times) {

        return String.join("", Collections.nCopies(times, "*"));
    }


}
