package s5;

public class Tack1 {

    public static void main(String[] args) throws InterruptedException {
        Her0 fox = new Her0(20);
        GodTimeHero rox = new GodTimeHero(50, 3000);
        rox.printStatus();
        fox.hit(rox);
        Thread.sleep(3000);
        rox.printStatus();
        fox.hit(rox);
        rox.printStatus();
    }
}