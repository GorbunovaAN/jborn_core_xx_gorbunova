package s5;

public class Multiplication extends Operation {


    @Override
    int calculate(int a, int b) {
        beforeLast = last;
        last = a * b;
        return last;
    }

}
