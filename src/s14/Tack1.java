package s14;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import java.util.Scanner;


public class Tack1 {
    public static void main(String[] args) throws InterruptedException, IOException {
        File file = new File("/home/alex/IdeaProjects/jborn_core_xx_gorbunova/src/s14/ha");
        String string = String.valueOf(readFile(file));
        StringBuilder stringBuilder = new StringBuilder(string.toLowerCase());



        ArrayList<SearchRunnable> searchRunnableArrayList = new ArrayList<>();
        for (char c = 'а'; c <= 'я'; c++) {
            searchRunnableArrayList.add(new SearchRunnable(stringBuilder, c));
        }

        Thread[] t = new Thread[searchRunnableArrayList.size()];
        for (int i = 0; i < searchRunnableArrayList.size(); i++) {
            Thread thread = new Thread(searchRunnableArrayList.get(i));
            thread.start();
            t[i] = thread;
        }
        for (Thread thread : t) {
            thread.join();
        }
        Comparator ltrComparator = new Comparator();
        searchRunnableArrayList.sort(ltrComparator);
        System.out.println("\nSorted:");
        for (SearchRunnable s : searchRunnableArrayList) {
            System.out.print(s.getLtr() + "" + s.getCount() + " ");
        }

    }

    public static StringBuilder readFile(File file) {
        StringBuilder str = new StringBuilder();
        try (FileReader fileReader = new FileReader(file);
             Scanner scan = new Scanner(fileReader)) {
            while (scan.hasNextLine()) {
                str.append(scan.nextLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}
