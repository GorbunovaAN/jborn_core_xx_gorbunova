package s14;

public class SearchRunnable implements Runnable {
    StringBuilder str;
    char ltr;
    private int count;


    public SearchRunnable(StringBuilder str, char ltr) {
        this.str = str;
        this.ltr = ltr;
    }

    public char getLtr() {
        return ltr;
    }

    public int getCount() {
        return count;
    }


    @Override
    public void run() {
        for (int i = 0; i < str.length(); i++)
            if (str.charAt(i) == ltr) {
                count++;
            }
    }
}