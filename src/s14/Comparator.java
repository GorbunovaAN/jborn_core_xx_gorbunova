package s14;

public class Comparator implements java.util.Comparator<SearchRunnable>{
        @Override
        public int compare(SearchRunnable o1, SearchRunnable o2) {
            return Integer.compare(o2.getCount(), o1.getCount());
        }
}